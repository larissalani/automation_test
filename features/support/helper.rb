# Classe para auxiliar em outputs, formatos
class Helper
  # def arquivo_csv(arquivo)
  #   CSV.foreach("features/support/csv/#{arquivo}", encoding: "iso-8859-1:utf-8", headers: true, col_sep: ";", converters: :all)
  # end

  # Formata o json para exibir em um formato mais legivel (vertical)
  def formata_json(json)
    begin
      JSON.pretty_generate(JSON.parse(json))  
    rescue JSON::ParserError => p
      raise RuntimeError.new("Nao foi possivel formatar o json \n #{p} ")
    end    
  end

  def compara_json(j1, j2)    
    resultado = JsonDiff.diff(j1, j2)    

    if !resultado.nil?
        raise CenarioFalhou.new("Erro na comparaçao de json: \n #{formata_json(resultado.to_json)}")
    end
  end

end

# Classe para falhar cenarios
class CenarioFalhou < StandardError
  def initialize(msg)
    super
  end
end

# Classe para criar erros a partir do response da requisicao
class DetalheErro
  def declara(response)
    request = response.request

    options_request = request.options

    @descricao = "Execuções que falharam ao efetuar a chamada: \n \n" 

    @header = "Headers utilizado na chamada: #{options_request[:headers] != nil ? Helper.new.formata_json(options_request[:headers].to_json) : nil} \n \n"
    
    @body = "Body utilizado na chamada:  #{options_request[:body] != nil ? Helper.new.formata_json(options_request[:body]) : nil} \n\n"

    @metodo = "Foi realizado o método do tipo => #{request.http_method.to_s.gsub('Net::HTTP::','').upcase()} \n \n"

    @header_reponse = "Header obtido na chamada: #{response.headers != nil ? Helper.new.formata_json(response.headers.to_json) : nil} \n \n"

    @chamada = "Chamada => #{request.last_uri.to_s} \n \n"

    @data = "Data do retorno obtido => #{Time.now.strftime("%d/%m/%Y %H:%M")} \n \n"

    @status_code = "Código do retorno obtido => #{response.code} \n \n"

    @erro = "Detalhes do erro: #{Helper.new.formata_json(response.body)} \n"
  end

  def imprime
    $output.puts @descricao + @header + @body + @metodo + @header_reponse + @chamada + @data + @status_code + @erro
  end
end

class HTTParty::Response
  def check_response_code(code)
    if self.code != code
      erro = DetalheErro.new
      erro.declara(self)
      erro.imprime
      fail "Status code incorreto! expect #{code} | got #{self.code}"
    end
  end
end
