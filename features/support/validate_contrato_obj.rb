class ValidateContratoObj
  def initialize(status_code, path_schema, response)
      @status_code = status_code
      @path_schema = path_schema
      @response = response
  end

  def valida_schema
      if @response.nil? then raise 'Nenhum contrato a ser validado'end
      
      if @path_schema == '' then raise 'O caminho do schema nao foi infomado' end

      if @response.code == @status_code
          ###### Validação do contrato
          
          begin
            errors = JSON::Validator.fully_validate(@path_schema, JSON.parse(@response.body))
          rescue
            raise StandardError.new("O arquivo '#{@path_schema}' de schema não foi encontrado")
          end
          
          for error in errors
            puts error
          end
          if errors.size > 0
            raise CenarioFalhou.new("Erro na validação do contrato #{errors}")            
          end
          
        end

  end 
end




class HTTParty::Response
def valida_schema(path_schema)    
  
  if self.code == 200
      
      begin
        errors = JSON::Validator.fully_validate(path_schema, JSON.parse(self.body))
      rescue Exception => e
        raise e
      rescue
        raise StandardError.new("O arquivo '#{path_schema}' de schema não foi encontrado")
      end
      
      for error in errors
        puts error
      end
      if errors.size > 0
        raise CenarioFalhou.new("Erro na validação do contrato #{errors}")            
      end
      
  end

end

end