class BuscaService

  include HTTParty

  base_uri CONFIG['uri_api']

  def post_intencao_busca(gerarIntencaoBuscaObj)
    @headers = {
      'Content-Type' => 'application/json'
      # 'Authorization' => CONFIG['token']
    }

    @body = gerarIntencaoBuscaObj.to_json

    @options = {
      :headers => @headers,
      :body => @body
    }

    self.class.post('/search', @options)
  end

  def get_busca(searchId, airlineName)

    @headers = {
      'Content-Type' => 'application/json'
      # 'Authorization' => CONFIG['token']
    }

    @options = {
      :headers => @headers
    }

    self.class.get("/search/#{searchId}/itineraries?airline=#{airlineName}")
  end

end