class GerarIntencaoBuscaObj
  attr_accessor :tripType, :from, :to, :adults, :children, :infants, :outboundDate, :inboundDate, :cabin

  def to_json
    {
      tripType: @tripType,
      from: @from,
      to: @to,
      adults: @adults,
      children: @children,
      infants: @infants,
      outboundDate: @outboundDate,
      inboundDate: @inboundDate,
      cabin: @cabin
    }.to_json
  end
end