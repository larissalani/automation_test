# Classe responsavel por montar o objeto de massa de dados
class GerarIntencaoBuscaBulkData
  class << self
    def gerar_intencao
      intencao_busca = GerarIntencaoBuscaObj.new
      intencao_busca.tripType = "OW"
      intencao_busca.from = "CGH"
      intencao_busca.to = "GIG"
      intencao_busca.adults = 1
      intencao_busca.children = 0
      intencao_busca.infants = 0
      intencao_busca.outboundDate = "2019-08-10"
      intencao_busca.inboundDate = "2019-09-29"
      intencao_busca.cabin = "EC"
      intencao_busca
    end
  end
end