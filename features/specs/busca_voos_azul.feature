#language: pt

Funcionalidade: Validar a buscas de voos da companhia azul
    Critérios de aceitação:
    Cada execução devera retornar com código de retorno compatível com o esperado
    Os contratos dos endpoints devem ser consistidos.

    Contexto:
        Dado que eu tenha dados validos para gerar a busca de voos
    
    Cenário: Validar a busca de voos somente ida pela classe econômica para a companhia Azul
        Dado que eu tenha uma intenção de busca somente ida pela classe econômica para a companhia Azul
        Quando realizo a busco de voos pelo searchId
        Entao valido se o retorno da busca está de acordo com o esperado
   
    Cenário: Validar a busca de voos ida e volta pela classe econômica para a companhia Azul
        Dado que eu tenha uma intenção de busca ida e volta pela classe econômica para a companhia Azul
        Quando realizo a busco de voos pelo searchId
        Entao valido se o retorno da busca está de acordo com o esperado

    Cenário: Validar a busca de voos somente ida pela classe executiva para a companhia Azul
        Dado que eu tenha uma intenção de busca somente ida pela classe executiva para a companhia Azul
        Quando realizo a busco de voos pelo searchId
        Entao valido se o retorno da busca está de acordo com o esperado