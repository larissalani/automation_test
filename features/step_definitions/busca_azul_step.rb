Dado("que eu tenha uma intenção de busca somente ida pela classe econômica para a companhia Azul") do
  puts "Payload \n #{@helper.formata_json(@@busca_bulk_data.to_json)}"

  resp_intecao_busca = $busca_service.post_intencao_busca(@@busca_bulk_data)

  resp_intecao_busca.check_response_code(200)

  resp_intecao_busca.valida_schema('features/schemas/intencao_busca_response_schema.json') 

  puts @helper.formata_json(resp_intecao_busca.body)

  @search_id = resp_intecao_busca['id']

  @airlineName = 'azul'
end

Dado("que eu tenha uma intenção de busca somente ida pela classe executiva para a companhia Azul") do
  @@busca_bulk_data.cabin = "EX"

  @@busca_bulk_data.outboundDate = "2019-08-06"

  puts @helper.formata_json(@@busca_bulk_data.to_json)

  resp_intecao_busca = $busca_service.post_intencao_busca(@@busca_bulk_data)

  resp_intecao_busca.check_response_code(200)

  resp_intecao_busca.valida_schema('features/schemas/intencao_busca_response_schema.json') 

  puts @helper.formata_json(resp_intecao_busca.body)

  @search_id = resp_intecao_busca['id']

  @airlineName = 'azul'
end

Dado("que eu tenha uma intenção de busca ida e volta pela classe econômica para a companhia Azul") do
  @@busca_bulk_data.tripType = "RT"

  @@busca_bulk_data.cabin = "EC"

  puts @helper.formata_json(@@busca_bulk_data.to_json)

  resp_intecao_busca = $busca_service.post_intencao_busca(@@busca_bulk_data)

  resp_intecao_busca.check_response_code(200)

  resp_intecao_busca.valida_schema('features/schemas/intencao_busca_response_schema.json') 

  puts @helper.formata_json(resp_intecao_busca.body)

  @search_id = resp_intecao_busca['id']

  @airlineName = 'azul'
end