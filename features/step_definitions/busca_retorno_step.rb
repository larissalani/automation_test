Dado("que eu tenha dados validos para gerar a busca de voos") do
  @@busca_bulk_data = GerarIntencaoBuscaBulkData.gerar_intencao
end

Quando("realizo a busco de voos pelo searchId") do
  @resp_busca = $busca_service.get_busca(@search_id, @airlineName)

  @resp_busca.check_response_code(200)

  @resp_busca.valida_schema('features/schemas/busca_response_schema.json')

  puts @helper.formata_json(@resp_busca.body)
end

Então("valido se o retorno da busca está de acordo com o esperado") do
  result_voos = @resp_busca['flights']

  puts "Total de voos encontrados: #{result_voos.size}"

  if result_voos.size != 0

    expect(result_voos[0]['airline']).to eq @airlineName

    expect(result_voos[0]['from']).to eq @@busca_bulk_data.from

    expect(result_voos[0]['to']).to eq @@busca_bulk_data.to

    expect(result_voos[0]['cabin']).to eq @@busca_bulk_data.cabin

    expect(Date.parse(result_voos[0]['departureDate'])).to eq Date.parse(@@busca_bulk_data.outboundDate)

    result_voos.each do |r|
      expect(r['airline']).to eq @airlineName

      expect(r['cabin']).to eq @@busca_bulk_data.cabin
    end
  else
    skip_this_scenario
  end
end