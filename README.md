# Arquitetura do projeto

## Tecnologia utilizada:
* Ruby - Versão: 2.5.3p105 (2018-10-18 revision 65156)
* HTTParty - GEM versão 0.17.0
* Cucumber - GEM versão 3.1.2

## Instalar e configurar o Ruby

## Instalar o gerenciador de pacotes bundler

 - Utilizar o comando:

=> gem install bundler

## Instalar as dependências necessárias no projeto:

 - Utilizar o comando:

=> bundle install

## Executar os testes com o comando

cucumber 
